@echo off

SET FUNCTION_NAME=pubsub_fire

REM Démarrer l'affichage des logs
gcloud logging read "resource.type=cloud_function AND resource.labels.function_name=%FUNCTION_NAME%" --limit=unlimited --format="value(textPayload,timestamp,severity)"
